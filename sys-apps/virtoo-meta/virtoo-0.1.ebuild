# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

SLOT="0"
KEYWORDS="~amd64 ~x86 arm64"

IUSE="bootstack kernel-sources wifi bluetooth ovirt cockpit docker lxc nvidia nouveau tegra"
HOMEPAGE="https://github.com/Azkali/virtoo"
DESCRIPTION="Meta package for all required packages for Virtoo"

# Prevent USE conflicts with different nvidia drivers
REQUIRED_USE="nouveau? ( !nvidia vaapi )"
REQUIRED_USE="nvidia? ( !nouveau vaapi )"
REQUIRED_USE="tegra? ( !nvidia !nouveau !bootstack !kernel-sources vaapi )"

RDEPEND+="bootstack? ( sys-boot/refind ) "													# REFind bootloader
RDEPEND+="kernel-sources? ( sys-kernel/gentoo-sources ) "									# Mainline kernel 

RDEPEND+="bluetooth? ( net-wireless/bluez ) "												# Bluetooth
RDEPEND+="wifi? ( net-misc/connman net-misc/dhcpcd net-vpn/openvpn ) "						# WiFi and DHCP

RDEPEND+="docker? ( app-emulation/docker ) "												# Docker
RDEPEND+="lxc? (app-emulation/lxc ) "														# LXC
RDEPEND+="virt? ( app-emulation/qemu sys-firmware/edk2-ovmf app-emulation/libvirt ) "		# QEMU and UEFI FW

RDEPEND+="ovirt? ( dev-db/postgresql app-emulation/ovirt-engine"							# oVirt WebUI
RDEPEND+="			app-emulation/ovirt-engine-dwh app-emulation/ovirt-engine-reports ) "

RDEPEND+="cockpit? ( app-admin/cockpit ) "													# Cockpit webUI

RDEPEND+="sys-kernel/linux-firmware net-misc/rsync dev-vcs/git "
